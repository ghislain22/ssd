#include <iostream>
#include <string>

using namespace std;

struct A {}; //arbitrary size

void f(A a)//unefficient
{}

void g(A& a) //better (non const)
{}

void g(const A& a)//better (param const)
{}

A h() //return by value : unefficient
{
    A a;
    return a;
}

A& hh() //wrong
{
    A a;
    return a;
}

void hh(A& a)
{
    //modifier a
}

void brol()
{
    string s = "Hello"; //dynamic alloc (slow)
    const char s2[] = "Hello"; //automatic (fast)
    //for you : StaticString<6> s2 = "Hello";
}

void brol2()
{
    string passwd8 = "";
    for(int i = 0; i < 8; i++)
        passwd8 += 'a';
    
    string passwd82(8, ' '); //construit "        ";
    for(int i = 0; i < 8; i++)
        passwd82[i] = 'a';
}

//DON'T EVER MAKE A NEW

int main()
{
    
}