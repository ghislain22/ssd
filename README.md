# Secure software development and web security - Romain Absil (rabsil@he2b.be)

The course is divided between the labs, and the _ex cathedra_ lessons.

The support for the lessons are slides, provided for several chapters. On the other hand, each lab requires the students to do some pratical work, such as providing an implementation.

To build the slides or the labs, refer to the readme files in the associated folders.

## License

This work is under Attribution-NonCommercial-ShareAlike 4.0 (CC BY-NC-SA 4.0).

Full legal code can be found on [Creative Commons](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode)
