#include <stdio.h>
#include <libintl.h>

int main(int argc, int** argv)
{
	if(argc > 1)
		printf(gettext("usage : %s filename \n"), argv[0]);
	else
		printf("Ok\n");
}
