#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int main(int c, char* args[])
{	
	if(c < 2)
	{
		printf("You must provide a file name\n");
		return 1;
	}

	const char * cat = "/bin/cat";
	char * cmd = malloc(strlen(cat) + strlen(args[1]) + 2);
	sprintf(cmd, "%s %s", cat, args[1]);

	setuid(0);
	system(cmd);
	setuid(getuid());	
}
